<?php
header("Content-type: application/json; charset=utf-8");
define('BASE_PATH', dirname(__FILE__));
// define('LECTIONARY_LANG','en_US');
// echo dirname(__FILE__);die;

require 'vendor/autoload.php';
require 'config.php';

use Lectionary\Lectionary;

#
$app = new \Slim\App(
    $config['slim']
);


        
// $log = new \App\Log;


// $app->add(function ($request, $response, $next) use ($log) {
//     return $next($request, $response);
// });

$Lec = new Lectionary($_GET['year']);

$app->get('/', function ($request, $response) use ($Lec){

header('Content-Type: application/json; charset=UTF-8');
    echo '<table>';
    foreach ($Lec->especialDays() as $key => $v) {
        if(!$v['observed'])
            $v['observed'] = 'Não utilizado este ano';

        echo '<tr><td>' . $v['date'] . ':</td><td><td><a href="' . $v['code'] . '">' . $v['liturgicalName'] . '</a></td><td><b>' . $key. '</b></td><td><b>' . $v['observed'] . '</b></td></tr>';
    }
    echo '</table>';
});

$app->get('/{day}',function($request, $response) use($Lec){
    $day = $request->getAttribute('day');

    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write($Lec->getDay($day));
    // return $next($request, $response);
});




 
 


$app->add(function ($request, $response, $next) {
    $uri = $request->getUri();
    $path = $uri->getPath();
    if ($path != '/' && substr($path, -1) == '/') {
        // permanently redirect paths with a trailing slash
        // to their non-trailing counterpart
        $uri = $uri->withPath(substr($path, 0, -1));
        
        if($request->getMethod() == 'GET') {
            return $response->withRedirect((string)$uri, 301);
        }
        else {
            return $next($request->withUri($uri), $response);
        }
    }

    return $next($request, $response);
});
 
$app->run();

